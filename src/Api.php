<?php

namespace HappeakApi\Pechkin;

use GuzzleHttp\Client;

class Api
{
    protected static $baseUrl = 'http://pechkin.happeak.net/';

    protected $authHeaders = [];

    /**
     * Api constructor.
     *
     * @param array $authHeaders
     */
    public function __construct($authHeaders = [])
    {
        $this->authHeaders = $authHeaders;
    }

    /**
     * @param array $authHeaders
     *
     * @return Api
     */
    public static function make($authHeaders = [])
    {
        return new static($authHeaders);
    }

    /**
     * @param array $endpoint
     *
     * @return Request
     */
    public function request($endpoint)
    {
        return (new Request($this))->endpoint($endpoint);
    }


    /**
     * @param Request $request
     *
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function call(Request $request)
    {
        $response = static::makeClient()->request($request->method, $request->endpoint, [
            'headers'     => array_merge($this->authHeaders, [
                'accept' => 'application/json',
            ]),
            'form_params' => [
                'params'      => $request->params,
                'attachments' => $request->attachments,
                'delay'       => $request->delay,
            ],
        ]);

        return json_decode($response->getBody(), true);
    }


    /**
     * @return Client
     */
    protected static function makeClient()
    {
        return new Client([
            'base_uri' => static::$baseUrl,
            'timeout'  => 5,
            'verify'   => false,
        ]);
    }

}