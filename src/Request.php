<?php


namespace HappeakApi\Pechkin;

class Request
{
    public $endpoint = null;
    public $params = [];
    public $attachments = [];
    public $delay = 0;
    public $method = 'GET';

    protected $api = null;

    public function __construct(Api $api)
    {
        $this->api = $api;
    }

    /**
     * @param mixed $name
     * @param mixed $value
     * @return Request
     */
    public function param($name, $value = null)
    {
        if (is_array($name))
        {
            $this->params = array_merge($this->params, $name);
        } else {
            $this->params[$name] = $value;
        }

        return $this;
    }

    /**
     * @param string $path
     * @param string $filename
     * @return Request
     */
    public function attachment($path, $filename)
    {
        if (is_readable($path))
        {
            $this->attachments[$filename] = base64_encode(file_get_contents($path));
        } else {
            throw new \InvalidArgumentException(sprintf('%s is not readable', $path));
        }

        return $this;
    }

    /**
     * Отложенная отправка письма
     *
     * @param $minutes
     *
     * @return $this
     */
    public function delay($minutes)
    {
        $this->delay = $minutes;

        return $this;
    }


    /**
     * @param array $params
     * @return Request
     */
    public function endpoint($params)
    {
        list($this->method, $this->endpoint) = $params;

        return $this;
    }

    /**
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function call()
    {
        return $this->api->call($this);
    }

}