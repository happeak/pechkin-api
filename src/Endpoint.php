<?php

namespace HappeakApi\Pechkin;

class Endpoint
{
    protected static $contactsIndex  = [
        'GET',
        '/api/contacts',
    ];
    protected static $contactsShow   = [
        'GET',
        '/api/contacts/%s',
    ];
    protected static $contactsSync   = [
        'POST',
        '/api/contacts/%s',
    ];
    protected static $contactsDelete = [
        'DELETE',
        '/api/contacts/%s',
    ];

    protected static $notifySend = [
        'POST',
        '/api/notify/%s',
    ];

    protected static $messagesIndex = [
        'GET',
        '/api/messages',
    ];
    protected static $messagesShow  = [
        'GET',
        '/api/messages/%s',
    ];

    /**
     * @return array
     */
    public static function contactIndex()
    {
        return self::$contactsIndex;
    }

    /**
     * @param string $email
     *
     * @return array
     */
    public static function contactShow($email)
    {
        return [
            self::$contactsShow[0],
            sprintf(self::$contactsShow[1], $email),
        ];
    }

    /**
     * @param string $email
     *
     * @return array
     */
    public static function contactSync($email)
    {
        return [
            self::$contactsSync[0],
            sprintf(self::$contactsSync[1], $email),
        ];
    }

    /**
     * @param string $email
     *
     * @return array
     */
    public static function contactDelete($email)
    {
        return [
            self::$contactsDelete[0],
            sprintf(self::$contactsDelete[1], $email),
        ];
    }


    /**
     * @param string $idKey
     *
     * @return array
     */
    public static function notifySend($idKey)
    {
        return [
            self::$notifySend[0],
            sprintf(self::$notifySend[1], $idKey),
        ];
    }

    /**
     * @return array
     */
    public static function messageIndex()
    {
        return [
            self::$messagesIndex,
        ];
    }

    /**
     * @param $email
     *
     * @return array
     */
    public static function messageShow($email)
    {
        return [
            self::$messagesShow[0],
            sprintf(self::$messagesShow[1], $email),
        ];
    }
}