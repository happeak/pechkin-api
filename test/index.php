<?php

require dirname(__FILE__).'/../vendor/autoload.php';

use HappeakApi\Pechkin\Api;
use HappeakApi\Pechkin\Endpoint;

$response = Api::make(['project-id' => 1, 'project-key' => 123])
    ->request(Endpoint::notifySend(2))
    ->param('email', 'rustam.miniakhmetov@happeak.com')
    ->attachment(dirname(__FILE__).'/attach.txt', 'somefile.txt')
    ->attachment(dirname(__FILE__).'/image.png', 'promo.png')
    ->call();

print_r($response);